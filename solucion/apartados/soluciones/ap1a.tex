En primer lugar descomponemos la solución tal que
\begin{equation}\label{eq:ap1a-u-xt}
	u(x,t) = X(x)T(t)
\end{equation}
y sustituimos en la ecuación~\eqref{eq:ap1-lineal}:
\begin{equation}\label{eq:ap1a-lineal-xt}
	\frac{1}{D}\frac{\partial XT}{\partial t} - \frac{\partial^2 XT}{\partial x^2} = \sigma(1 + XT)
\end{equation}

Primero resolvemos la parte homogénea del problema, es decir,
\begin{gather*}
	\frac{1}{D}\frac{\partial XT}{\partial t} - \frac{\partial^2 XT}{\partial x^2} = \sigma XT
	\\
	\frac{1}{D}T'X - X''T = \sigma
	\\
	\frac{1}{D}\frac{T'}{T} = \sigma + \frac{X''}{X} = \lambda^2
\end{gather*}
y comenzamos resolviendo la parte espacial del sistema
\begin{gather*}
	\sigma + \frac{X''}{X} = \lambda^2 \implies \frac{X''}{X} + \sigma - \lambda^2 = 0
	\\
	X'' + (\sigma - \lambda^2)X = 0
\end{gather*}
Ensayamos una solución de la forma $X=e^{\alpha x}$, entonces tenemos:
\begin{gather*}
	\alpha^2 e^{\alpha x} + (\sigma - \lambda^2) e^{\alpha x} = 0 \implies \alpha = \pm \sqrt{\lambda^2 - \sigma}
\end{gather*}
Ahora tenemos tres casos para los valores de $\alpha$.
\begin{itemize}
	\item Si $\alpha = 0$:
	\begin{align*}
		X(x) &= c_1 e^{\alpha x} + c_2 x e^{\alpha x} = c_1 + c_2 x
		\\
		X(0) &= c_1 = 0
		\\
		X(1) &= c_2 = 0
	\end{align*}
	Como vemos, se obtiene una solución $X(x) = 0$ la cual no es válida.
	
	\item Si $\alpha > 0$:
	\begin{align*}
		X(x) &= c_1 \cosh(\sqrt{\lambda^2 - \sigma} x) + c_2 \sinh(\sqrt{\lambda^2 - \sigma} x)
		\\
		X(0) &= c_1 \cosh(0) + c_2 \sinh(0) = 0 \implies c_1 = 0
		\\
		X(1) &= c_2 \sinh(\sqrt{\lambda^2 - \sigma}) = 0 \implies c_2 = 0
	\end{align*}
	Esto también nos lleva a $X(x) = 0$, por lo que no es una solución válida.
	
	\item Si $\alpha < 0$:
	\begin{align*}
		X(x) &= c_1 \cos(\sqrt{\lambda^2 - \sigma} x) + c_2 \sin(\sqrt{\lambda^2 - \sigma} x)
		\\
		X(0) &= c_1 \cos(0) + c_2 \sin(0) = 0 \implies c_1 = 0
		\\
		X(1) &= c_2 \sin(\sqrt{\lambda^2 - \sigma}) = 0 \implies \sqrt{\lambda^2 - \sigma} = n\pi
	\end{align*}
	De forma que nuestra solución tomaría la forma $X(x) = \sum_{n=1}^{\infty} \sin(n\pi x)$, que sí es válida.
\end{itemize}

Ahora sustituimos $X(x)$ en la ecuación~\eqref{eq:ap1a-lineal-xt}
\begin{align*}
	\frac{1}{D} \sum_{n=1}^{\infty} \sin(n\pi x) T_{n}' - \left( - \sum_{n=1}^{\infty} \sin(n\pi x) n^2 \pi^2 T_n \right) = \sigma (1 + \sum_{n=1}^{\infty} \sin(n\pi x) T_n)
\end{align*}
y reorganizamos términos
\begin{align} \label{eq:ap1a-lineal-xt2}
	\frac{1}{D} \sum_{n=1}^{\infty} \sin(n\pi x) T_{n}' - \left( - \sum_{n=1}^{\infty} \sin(n\pi x) n^2 \pi^2 T_n \right) - \sigma \sum_{n=1}^{\infty} \sin(n\pi x) T_n = \sigma
\end{align}
donde, para poder eliminar factores comunes y simplificar la expresión con el objetivo de resolver la EDO para $T_n$, debemos obtener la transformada de Fourier de $\sigma$ en senos, es decir,
\begin{align*}
	\sigma = \sum_{n=1}^{\infty} b_n \sin(n\pi x)
\end{align*}
donde $b_n$ se calcula tal que
\begin{align*}
	b_n =& \int_{-1}^{0} - \sigma sin(n\pi x)\ dx + \int_{0}^{1} \sigma sin(n\pi x)\ dx = 
	\\
	    & \left[\frac{-\sigma}{n\pi}(-\cos(n\pi x))\right]_{-1}^{0} + \left[\frac{\sigma}{n\pi}(-\cos(n\pi x))\right]_{0}^{1} = 
	\\
	    & \left[\frac{\sigma}{n\pi} - \frac{\sigma}{n\pi}(-1)^n\right] + \left[\frac{-\sigma}{n\pi}(-1)^n - \frac{-\sigma}{n\pi}\right] = 
	\\
	    & \frac{\sigma}{n\pi}\left(1 - (-1)^n\right) + \frac{\sigma}{n\pi}\left(1 - (-1)^n\right) \implies
	\\
	b_n =& \frac{2\sigma}{n\pi} \left(1 - (-1)^n\right)
\end{align*}
por lo que obtenemos
\begin{align*}
	\sigma = \sum_{n=1}^{\infty} \frac{2\sigma}{n\pi} \left(1 - (-1)^n\right) \sin(n\pi x)
\end{align*}

Finalmente sustituimos en la ecuación~\eqref{eq:ap1a-lineal-xt2}
\begin{multline*}
	\frac{1}{D} \sum_{n=1}^{\infty} \sin(n\pi x) T_{n}' + \sum_{n=1}^{\infty} \sin(n\pi x) n^2 \pi^2 T_n - \sigma \sum_{n=1}^{\infty} \sin(n\pi x) T_n = 
	\sum_{n=1}^{\infty} \sin(n\pi x) \frac{2\sigma}{n\pi} \left(1 - (-1)^n\right)
\end{multline*}
donde podemos eliminar los factores comunes, por lo que nos queda la siguiente EDO:
\begin{gather*}
	\frac{1}{D} T_{n}' + n^2 \pi^2 T_n - \sigma T_n = \frac{2\sigma}{n\pi} \left(1 - (-1)^n\right)
	\\
	\frac{1}{D} T_{n}' + (n^2 \pi^2 - \sigma) T_n = \frac{2\sigma}{n\pi} \left(1 - (-1)^n\right)
	\\
	T_{n}' = D \left(\frac{2\sigma}{n\pi} (1 - (-1)^n) - T_n (n^2 \pi^2 - \sigma) \right)
\end{gather*}
a la que aplicamos el siguiente cambio de variables para simplificar la operación
\begin{gather*}
	a = \frac{2\sigma}{n\pi} (1 - (-1)^n)
	\\
	b = n^2 \pi^2 - \sigma
\end{gather*}
entonces
\begin{gather*}
	\frac{dT}{dt} = D(a-T_n b) \implies \frac{\frac{dT}{dt}}{a-b} = D
	\\
	\int \frac{\frac{dT}{dt}}{a-T_n b}\ dt = \int D\ dt
	\\
	\log(a - T_nb) \frac{-1}{b} = Dt + c_1
	\\
	\log(a - T_nb) = -b (Dt + c_1)
	\\
	a - T_n b = e^{-Dtb} e^{-bc_1}
	\\
	T_n = \frac{a}{b} - \frac{e^{-Dtb} e^{-bc_1}}{b}
\end{gather*}
y deshaciendo el cambio de variables llegamos a:
\begin{gather*}
	T_n = \frac{2\sigma(1 - (-1)^n)}{n\pi(n^2 \pi^2 - \sigma)} - \frac{e^{-Dt(n^2 \pi^2 - \sigma)} e^{-c_1(n^2 \pi^2 - \sigma)}}{n^2 \pi^2 - \sigma}
\end{gather*}
donde podemos considerar la constante $c_n = e^{-c_1(n^2 \pi^2 - \sigma)}$, por lo que nos queda:
\begin{gather*}
	T_n = \frac{2\sigma(1 - (-1)^n)}{n\pi(n^2 \pi^2 - \sigma)} - e^{-Dt(n^2 \pi^2 - \sigma)} \frac{c_n}{n^2 \pi^2 - \sigma}
\end{gather*}

Por último, sustituimos $X(x)$ y $T(t)$ en la expresión~\eqref{eq:ap1a-u-xt}
\begin{equation}\label{eq:ap1a-u-xt-c2}
	u(x,t) = \sum_{n=1}^{\infty} \sin(n\pi x) \left(\frac{2\sigma(1 - (-1)^n)}{n\pi(n^2 \pi^2 - \sigma)} - e^{-Dt(n^2 \pi^2 - \sigma)} \frac{c_n}{n^2 \pi^2 - \sigma}\right)
\end{equation}
donde para determinar el valor de $c_n$ debemos utilizar las condiciones iniciales y descomponer $u_0$ como serie de Fourier en senos:
\begin{gather*}
	u(x,0) = \sum_{n=1}^{\infty} \sin(n\pi x) \left( \frac{2\sigma(1 - (-1)^n)}{n\pi(n^2 \pi^2 - \sigma)} - \frac{c_n}{n^2 \pi^2 - \sigma} \right) = u_0(x)
\end{gather*}

Entonces, para $u_0(x)$ tenemos:
\begin{gather*}
	u_0(x) = \sum_{n=1}^{\infty} b_n \sin(n\pi x)
	\\
	b_n = \int_{-\infty}^{+\infty} u_0(x) \sin(n\pi x)\ dx
	\\
	u_0(x) = \sum_{n=1}^{\infty} \sin(n\pi x) \int_{-1}^{+1} u_0(x) \sin(n\pi x)\ dx
\end{gather*}
por lo que
\begin{gather*}
	\sum_{n=1}^{\infty} \sin(n\pi x) \left( \frac{2\sigma(1 - (-1)^n)}{n\pi(n^2 \pi^2 - \sigma)} - \frac{c_n}{n^2 \pi^2 - \sigma} \right) = \sum_{n=1}^{\infty} \sin(n\pi x) \int_{-1}^{+1} u_0(x) \sin(n\pi x)\ dx
	\\
	\frac{2\sigma(1 - (-1)^n)}{n\pi(n^2 \pi^2 - \sigma)} - \frac{c_n}{n^2 \pi^2 - \sigma} = \int_{-1}^{+1} u_0(x) \sin(n\pi x)\ dx
	\\
	- \frac{c_n}{n^2 \pi^2 - \sigma} = \int_{-1}^{+1} u_0(x) \sin(n\pi x)\ dx - \frac{2\sigma(1 - (-1)^n)}{n\pi(n^2 \pi^2 - \sigma)}
	\\
	c_n = \frac{2\sigma(1 - (-1)^n)}{n\pi} - (n^2 \pi^2 - \sigma)\int_{-1}^{+1} u_0(x) \sin(n\pi x)\ dx
\end{gather*}

Sustituyendo en la ecuación~\eqref{eq:ap1a-u-xt-c2} obtenemos:
\begin{multline*}
	u(x,t) = \sum_{n=1}^{\infty} \sin(n\pi x) 
	\Biggl(
		\frac{2\sigma(1 - (-1)^n)}{n\pi(n^2 \pi^2 - \sigma)}
		\\
		- e^{-Dt(n^2 \pi^2 - \sigma)}
		\left(
			\frac{2\sigma(1 - (-1)^n)}{n\pi(n^2 \pi^2 - \sigma)}
			- \int_{-1}^{+1} u_0(x) \sin(n\pi x)\ dx
		\right)
	\Biggr)
\end{multline*}
y reorganizando términos y signos, llegamos a la solución final
\begin{multline} \label{eq:ap1a-solved}
	u(x,t) = \sum_{n=1}^{\infty} \sin(n\pi x) 
	\Biggl(
		\frac{2\sigma(1 - (-1)^n)}{n\pi(n^2 \pi^2 - \sigma)}
		\\
		+ e^{-Dt(n^2 \pi^2 - \sigma)}
		\left(
			\int_{-1}^{+1} u_0(x) \sin(n\pi x)\ dx
			- \frac{2\sigma(1 - (-1)^n)}{n\pi(n^2 \pi^2 - \sigma)}
		\right)
	\Biggr)
\end{multline}
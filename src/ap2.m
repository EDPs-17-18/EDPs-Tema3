function [ U, T, X ] = ap2a(Tmax, Nt, Xmax, Nx)
    %   each column of U represent one time step, one row represent one spatial step
    %   Tmax = max time value
    %   Nt = number of time steps
    %   Xmax = max spacial value
    %   Nx = number of spacial steps
    %

    m = 0;
    X=linspace(0,Xmax,Nx);
    T=linspace(0,Tmax,Nt);

    sol = pdepe(m,@pde_pde,@pde_ic,@pde_bc,X,T);
    U = sol(:,:,1);
    surf(X,T,U);
    % shading interp
% end  % function


% c*Ut = f*Ux + s
% PDE factors
function [c,f,s] = pde_pde(x,t,u,DuDx)
    D = 1;
    sigma = 4;
    c = 1/D;
    f = DuDx;
    s = sigma*exp(u);

% Initial conditions
function u0 = pde_ic(x)
    % u0 = 0;
    u0 = 4*x*(1-x);
    % u0 = 40*x*(1-x);

% Boundary conditions
function [pl,ql,pr,qr] = pde_bc(xl,ul,xr,ur,t)
    pl = ul;
    ql = 0;
    pr = ur;
    qr = 0;

function [ eige ] = ap3f( Xs )
    sigma = 2;
    D = 1;

    nx = length(Xs);
    h = abs(Xs(2) - Xs(1));

    d = D/h^2;
    c = @(x) D*sigma*exp(us(x)) - 2*D/h^2;
    e = D/h^2;

    A = eye(nx,nx);
    A(1,1) = c(Xs(1));
    A(1,2) = e;
    for i=2:nx-1
        A(i,i-1) = d;
        A(i,i) = c(Xs(i));
        A(i,i+1) = e;
    end
    A(nx,nx-1) = d;
    A(nx,nx) = c(Xs(nx));

    % A;
    eige = eig(A);
    max(real(out))
% end  % function


function u = us(x)
    % for lower value set guesses=1, for major value set guesses=10
    initial_guess_p0 = 10;
    initial_guess_a = 10;
    initial_guess_b = 10;
    sigma = 2;
    % finding p0, and the parameters a and b for the estacionary solution
    p0 = shoot_method(sigma, initial_guess_p0);
    b = fzero(@(b) 2.*b.*(tanh(b-b/2))-p0, initial_guess_b);
    as = fzero(@(a) a - 2*log(cosh(b.*(1/2))), initial_guess_a);
    us = @(x, a, b) a - 2.*log(cosh(b.*(x - 1/2)));
    % value for a in u_0 equation
    % a = +0.01;
    % value of u0 = us + a*x*(1-x)
    u = us(x,as,b); % + a.*x.*(1-x);

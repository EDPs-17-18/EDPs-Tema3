function [ U ] = ap1( Xs, Ts )
    % each column of U represent one time step, one row represent one spatial step
    % Xs = linspace of X
    % Ts = linspace of T

    n = length(Xs);
    m = length(Ts);
    for j=1:m
        for i=1:n
            U(i,j) = u_func(Xs(i),Ts(j));
        end
    end

    surf(Ts,Xs,U);
% end  % ap1

function [u] = u_func(x, t)
    D = 1;
    sigma = 4;
    max_sum = 25;

    u0 = @(x) 0;
    % u0 = @(x) 4.*x.*(1-x);
    % u0 = @(x) 40.*x.*(1-x);

    integral_1 = @(n,y) sin(n.*pi.*y) .* u0(y);

    first_contrib = 0;
    secnd_contrib = 0;

    u = 0;

    for n=1:max_sum
        % first_contrib = sin(n*pi*x) * (2*sigma*(1 - (-1)^n))/(n*pi*(n^2*pi^2 - sigma))
        first_contrib = first_contrib + sin(n*pi*x) * (2*sigma*(1 - (-1)^n))/(n*pi*(n^2*pi^2 - sigma));

        % secnd_contrib = sin(n*pi*x) * exp(-D*t*(n^2*pi^2 - sigma)) * (integral(@(y)integral(n,y), -1, 1)) - (2*sigma*(1 - (-1)^n))/(n*pi*(n^2*pi^2 - sigma));
        secnd_contrib = secnd_contrib + sin(n*pi*x) * exp(-D*t*(n^2*pi^2 - sigma)) * (integral(@(y)integral_1(n,y), 0, 1) - (2*sigma*(1 - (-1)^n))/(n*pi*(n^2*pi^2 - sigma)));
    end

    u = first_contrib + secnd_contrib;
% end % u_func

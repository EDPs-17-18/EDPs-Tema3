function [ U, T, X ] = ap3a(Tmax, Nt, Xmax, Nx)
    %   each column of U represent one time step, one row represent one spatial step
    %   Tmax = max time value
    %   Nt = number of time steps
    %   Xmax = max spacial value
    %   Nx = number of spacial steps
    %

    m = 0;
    X=linspace(0,Xmax,Nx);
    T=linspace(0,Tmax,Nt);

    sol = pdepe(m,@pde_pde,@pde_ic,@pde_bc,X,T);
    U = sol(:,:,1);
    surf(X,T,U);
    % shading interp
% end  % function


% c*Ut = f*Ux + s
% PDE factors
function [c,f,s] = pde_pde(x,t,u,DuDx)
    D = 1;
    sigma = 2;
    c = 1/D;
    f = DuDx;
    s = sigma*exp(u);

% Initial conditions
function u0 = pde_ic(x)
    % for lower value set guesses=1, for major value set guesses=10
    initial_guess_p0 = 1;
    initial_guess_a = 1;
    initial_guess_b = 1;
    sigma = 2;
    % finding p0, and the parameters a and b for the estacionary solution
    p0 = shoot_method(sigma, initial_guess_p0);
    b = fzero(@(b) 2.*b.*(tanh(b-b/2))-p0, initial_guess_b);
    as = fzero(@(a) a - 2*log(cosh(b.*(1/2))), initial_guess_a);
    us = @(x, a, b) a - 2.*log(cosh(b.*(x - 1/2)));
    % value for a in u_0 equation
    a = +0.01;
    % value of u0 = us + a*x*(1-x)
    u0 = us(x,as,b) + a.*x.*(1-x);

% Boundary conditions
function [pl,ql,pr,qr] = pde_bc(xl,ul,xr,ur,t)
    pl = ul;
    ql = 0;
    pr = ur;
    qr = 0;
